﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System;

public class GameManager : MonoBehaviour {

	CreatePanel panelManager;
	int panelCount = 0;
	bool startFlag = false;
	bool panelOpenedFlag = false;
	bool winnerPanelRemovedFlag = false;
	
	Color[] colors = new Color[]{Color.red, Color.blue, Color.yellow, Color.green};
	
	// パネルに置く順番。正答率が高いほど高頻度でlistに追加されている
	List<Color> putColorList = null;
	
	// input fields
	public GameObject redCorrectAnswerNumField;
	public GameObject blueCorrectAnswerNumField;
	public GameObject yellowCorrectAnswerNumField;
	public GameObject greenCorrectAnswerNumField;
	
	
	// result text
	public GameObject redResultText;
	public GameObject blueResultText;
	public GameObject yellowResultText;
	public GameObject greenResultText;
	
	public GameObject image;
	
	void Start () {
		panelManager = gameObject.GetComponent<CreatePanel>();
	}
	
	// Update is called once per frame
	void Update () {
		if(putColorList == null || !startFlag){ return; }
		
		if(putColorList.Count <= 0 && !winnerPanelRemovedFlag){
			showResult();
			return;
		}
		
		if(!putPanel()){
			panelOpenedFlag = true;
			putColorList.Clear();
		}
	}
	
	private bool putPanel(){
		Vector2 pos = panelManager.getEmptyPanel();
		if(pos.x == -1f && pos.y == -1f){ return false; }
		
		panelManager.changePanelColor((int)pos.x, (int)pos.y, getColorRandom());
		panelCount++;
		return true;
	}

	public void StartGame(){
		if(!startFlag){initPutColorList();}
		startFlag = true;
	}

	public void ResetGame(){
		startFlag = false;
		winnerPanelRemovedFlag = false;
		panelOpenedFlag = false;
		putColorList.Clear();// nullにするとUpdateの挙動がおかしくなる
		panelManager.Reset();
	}

	private int[] createCorrectAnswerNumList(List<GameObject> objs){
		return objs.Select(o => Int32.Parse(o.GetComponent<InputField>().text)).ToArray();
	}

	private int[] calcCorrectAnswerPercent(int[] answers){
		float sum = answers.Sum();
		
		var ret = new int[answers.Length];
		for(int i = 0; i < answers.Length; i++){
			ret[i] = (int)System.Math.Round((double)(answers[i] / sum * 100f));
		}
		
		// 合計が100になるかチェック
		int perSum = ret.Sum();
		
		// 合計を100に揃える
		int cnt = 0;
		while(perSum != 100){
			if(perSum > 100){
				ret[cnt % ret.Length]--;
				perSum--;
			}else if(perSum < 100){
				ret[cnt % ret.Length]++;
				perSum++;
			}
			cnt++;
		}
		return ret;
	}
	
	private void initPutColorList(){
		var correctAnswerNumFields = createCorrectAnswerNumFields();
		int[] correctAnswerNums = createCorrectAnswerNumList(correctAnswerNumFields);
		int[] panelRate = calcCorrectAnswerPercent(correctAnswerNums);
		putColorList = createPutColorList(panelRate);
	}
	
	private List<Color> createPutColorList(int [] panelRates){
		var putColorList = new List<Color>();
		for(int i = 0; i < panelRates.Length; i++){
			for(int j = 0; j < panelRates[i]; j++){
				putColorList.Add(colors[i]);
			}
		}
		return putColorList;
	}
	
	private Color getColorRandom(){
		return putColorList[UnityEngine.Random.Range(0, putColorList.Count)];
	}
	
	private List<GameObject> createCorrectAnswerNumFields(){
		var correctAnswerNumFields = new List<GameObject>();
		correctAnswerNumFields.Add(redCorrectAnswerNumField);
		correctAnswerNumFields.Add(blueCorrectAnswerNumField);
		correctAnswerNumFields.Add(yellowCorrectAnswerNumField);
		correctAnswerNumFields.Add(greenCorrectAnswerNumField);
		return correctAnswerNumFields;
	}
	
	private Dictionary<Color, GameObject> createResultTexts(){
		var resultTexts = new Dictionary<Color, GameObject>();
		resultTexts.Add(Color.red, redResultText);
		resultTexts.Add(Color.blue, blueResultText);
		resultTexts.Add(Color.yellow, yellowResultText);
		resultTexts.Add(Color.green, greenResultText);
		return resultTexts;
	}
	
	private void showResult(){
		var results = panelManager.GetPanelsNum();
		var resultTexts = createResultTexts();
		foreach(KeyValuePair<Color, int> result in results){
			GameObject textObj = resultTexts[result.Key];
			textObj.GetComponent<Text>().text = result.Value.ToString();
		}
	}
	
	public void RemoveWinnerPanels(){
		if(!panelOpenedFlag || winnerPanelRemovedFlag){ return; }

		Vector2 scale =  calcResultImageScale();
		float pos = panelManager.PanelSize/2;
		var imageObject = (GameObject)Instantiate(image, new Vector3(-pos, -pos), Quaternion.identity);
		imageObject.transform.localScale = new Vector3(scale.x, scale.y); 
		var color = panelManager.GetPanelsNum()
					.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
		panelManager.RemovePanels(color);
		winnerPanelRemovedFlag = true;
	}
	
	private Vector2 calcResultImageScale(){
		var imageRect = panelManager.GetPanelsSize();
		var sr = image.GetComponent<SpriteRenderer>();
		var imageWidth  = sr.bounds.size.x;
		var imageHeight = sr.bounds.size.y;
		return new Vector2(imageRect.x / imageWidth, imageRect.y / imageHeight);
	}
}
