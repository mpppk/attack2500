﻿using UnityEngine;

public class ResetGameButton : MonoBehaviour {
	public GameObject gameManager;

	public void OnClick(){
		Debug.Log("reset!!");
		gameManager.GetComponent<GameManager>().ResetGame();
	}
}
