﻿using UnityEngine;

public class ResultButton : MonoBehaviour {
	public GameObject gameManager;

	public void OnClick(){
		Debug.Log("start game!!");
		gameManager.GetComponent<GameManager>().RemoveWinnerPanels();
	}
}
