﻿using UnityEngine;
using System.Collections;

public class StartGameButton : MonoBehaviour {
	public GameObject gameManager;

	public void OnClick(){
		Debug.Log("start game!!");
		gameManager.GetComponent<GameManager>().StartGame();
	}
}
