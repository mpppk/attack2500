
public class NearestPanelsPosition{
	public int top;
	public int bottom;
	public int left;
	public int right;
	
	public NearestPanelsPosition(int top, int bottom, int left, int right){
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
	}
	
	public NearestPanelsPosition(){}
}