
using UnityEngine;
using System.Collections;
 
// メッシュ色変更
public class SetMeshColor : MonoBehaviour {
    public Color color;    // 変更後のメッシュの色
 
    void Update() {
        // メッシュの色を変更
        var mesh = GetComponent<MeshFilter>().mesh;
        var colors = mesh.colors;
        Debug.Log("set mesh color");
        for ( int i = 0 ; i < mesh.colors.Length ; ++i ) {
            Debug.Log("mesh num: " + i);
            colors[i] = color;
        }
 
        mesh.colors = colors;
    }
}