﻿using UnityEngine;
using System.Collections.Generic;

public class CreatePanel : MonoBehaviour {
	public GameObject panelPrefab;
	public float PanelSize {
		get;
		private set;
	}
	private int maxRow = 50;
	private int maxCol = 50;
	GameObject[,] panels;
	
	List<Vector2> emptyPanels = new List<Vector2>();
	// Use this for initialization
	void Start () {
		PanelSize = 0.15F;
		createPanels();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	private void createPanels(){
		var marginX = -1F * PanelSize * maxCol / 2;
		var marginY = -1F * PanelSize * maxRow / 2;
		
		panels = new GameObject[maxRow, maxCol];
		
		for(int i = 0; i < maxCol; i++){
			for(int j = 0; j < maxRow; j++){
				panels[i, j] = Instantiate(panelPrefab, new Vector3(marginX + i * PanelSize, marginY + j * PanelSize, 0), Quaternion.identity) as GameObject;
				panels[i, j].name = i + "," + j;
				emptyPanels.Add(new Vector2(i, j));
			}
		}
	}
	
	public void removePanels(){
		foreach(GameObject panel in panels){
			if(panel != null){Destroy(panel);}
		}
	}
	
	public void changePanelColor(int x, int y, Color color, bool changeOtherFlag = true){
		// 指定されたpanelの色を変更
		panels[x, y].GetComponent<Renderer>().material.color = color;
		emptyPanels.RemoveAll( vec => vec.x == x && vec.y == y);
		if(!changeOtherFlag){ return; }
		
		NearestPanelsPosition pos = searchNearestPanel(x, y, color);
		fillPanels(x, y, color, pos);
	}
	
	// 指定したパネルの上下左右でそれぞれ最も近い同色のパネルを返す
	private NearestPanelsPosition searchNearestPanel(int x, int y, Color color){
		NearestPanelsPosition pos = new NearestPanelsPosition(-1, -1, -1, -1);
		// 上を探す
		for(int i = y - 1; i >= 0; i--){
			if(hasColor(x, i, color)){
				pos.top = i;
				break;
			}
		}
		
		// 下を探す 
		for(int i = y + 1; i < maxRow; i++){
			if(hasColor(x, i, color)){
				pos.bottom = i;
				break;
			}
		}
		
		// 指定されたパネルの左側に存在する、最も近い同色のパネルを探す
		for(int i = x - 1; i >= 0; i--){
			if(hasColor(i, y, color)){
				pos.left = i;
				break;
			}
		}

		// 右側を探す
		for(int i = x + 1; i < maxCol; i++){
			if(hasColor(i, y, color)){
				pos.right = i;
				break;
			}
		}
		
		return pos;
	}
	
	private void fillPanels(int x, int y, Color color, NearestPanelsPosition pos){
		if(pos.left   != -1){ fillPanels(pos.left, x,         y,       y+1,        color); }
		if(pos.right  != -1){ fillPanels(x,        pos.right, y,       y+1,        color); }
		if(pos.top    != -1){ fillPanels(x,        x+1,       pos.top, y,          color); }
		if(pos.bottom != -1){ fillPanels(x,        x+1,       y,       pos.bottom, color); }
	}
	
	private void fillPanels(int startX, int endX, int startY, int endY, Color color){
		for(int x = startX; x < endX; x++){
			for(int y = startY; y < endY; y++){
				panels[x, y].GetComponent<Renderer>().material.color = color;
				emptyPanels.RemoveAll( vec => vec.x == x && vec.y == y);
			}
		}
	}
	
	private bool hasColor(int x, int y, Color color){
		Color panelColor = getPanelColor(panels[x, y]);
		return panelColor == color;
	}
	
	static private Color getPanelColor(GameObject panel){
		return panel.GetComponent<Renderer>().material.color;
	}
	
	public Vector2 getEmptyPanel(){
		// TODO 例外の方がいいかも
		if(emptyPanels.Count <= 0){ return new Vector2(-1, -1); }
		int index = UnityEngine.Random.Range(0, emptyPanels.Count);
		return emptyPanels[index];
	}
	
	public void Reset(){
		emptyPanels.Clear();
		removePanels();
		createPanels();
	}
	
	public Dictionary<Color, int> GetPanelsNum(){
		var ret = new Dictionary<Color, int>();
		foreach(GameObject panel in panels){
			Color color = getPanelColor(panel);
			if(ret.ContainsKey(color)){
				ret[color]++;
			}else{
				ret.Add(color, 0);
			}
		}
		return ret;
	}
	
	public void RemovePanels(Color color){
		foreach(GameObject panel in panels){
			Color currentPanelColor = getPanelColor(panel);
			if(panel != null && currentPanelColor == color){ Destroy(panel); }
		}		
	}
	
	public Vector2 GetPanelsSize(){
		return new Vector2(PanelSize * maxCol, PanelSize * maxRow);
	}
	
}
