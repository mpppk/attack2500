using UnityEngine;
using System.Collections;
 
//要するに今回は三角形を作っていくよ。
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class CreateMesh : MonoBehaviour
{
    [SerializeField]
    //頂点（作る図形は三角形だから３つ必要）
    Vector3[] vertices = {
        new Vector3(-10,0,0),//左下の頂点
        new Vector3(10,0,0),//右下の頂点
        new Vector3(0,0,10)//上の頂点
    };
    [SerializeField]
    //画像の端と頂点を合わせるけど、四角ではないので画像は欠損する
    Vector2[] UV = {
                     new Vector2(0,0), new Vector2(1,0), new Vector2(0.5f,1)  
                   };
     
    [SerializeField]
    //頂点インデックス(作られるポリゴン数＊3)
    int[] triangles = {
                       0,2,1//左下＝＞上＝＞右下の順   
                      };
 
    // Use this for initialization
    void Start()
    {
        //メッシュを作成
        Mesh mesh = new Mesh();
        //メッシュにわかりやすいよう名前をつける
        mesh.name = "MyMesh";
 
        //メッシュに頂点情報を設定
        mesh.vertices = vertices;
 
        //メッシュにUV情報を設定
        mesh.uv = UV;
         
        //メッシュに頂点インデックスを設定
        mesh.triangles = triangles;
 
        //法線、バウンディングボリュームを自動計算
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
 
        //メッシュフィルター（描画）とメッシュコライダー（当たり判定）にメッシュを代入
        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }
}